<?php

$age = 17;
$people_number = 2;

if($age >= 18) {
    echo "You have $age years old. You can enter alone.";
} else if ($age >=16 && $people_number > 1) {
    echo "You have $age years old. You can enter because you are accompanied.";
} else {
    echo "You can't enter"; 
}