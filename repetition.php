<?php

// While
$j = 0;

while($j < 15) {

    echo $j . PHP_EOL;
    $j++;
}

// For
for($i = 10; $i > 0; $i--) {

    if ($i == 9) {
        continue;
    }
    echo $i . PHP_EOL;
}