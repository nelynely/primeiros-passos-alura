<?php

echo "Informe o peso: ";
$peso=rtrim(fgets(STDIN));

echo "Informe a altura: ";
$altura=rtrim(fgets(STDIN));

$imc = $peso / ($altura ** 2);

if ($imc < 18.5) {
    echo "Abaixo do peso: ";
} else if ($imc >= 18.5 && $imc <= 25.9) {
    echo "Peso nomal: ";
} else if ($imc >= 25 && $imc <= 29.9) {
    echo "Sobrepeso: ";
} else if ($imc >= 30 && $imc <= 34.9) {
    echo "Obesidade grau 1: ";
} else if ($imc >= 35 && $imc <= 39.9) {
    echo "Obesidade grau 2: ";
} else {
    echo "Obesidade grau 3: ";
}